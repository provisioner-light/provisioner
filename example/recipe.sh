# Protect secrets from reading by any other
umask 077

# Decrypt secret domains
pass show company/ssh > ./output/user/.ssh/config.d/company

# Fill secrets for vpn tunnel
pass show company/vpn | all_secrets_fill ./output/root/etc/ipsec.conf
