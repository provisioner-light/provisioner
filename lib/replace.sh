# Functions to fill secrets in configuration templates
# Usefull in recipies scripts


# Find and replace all occurences of given string in given file
# Replacement string is read from standard input
#
# Main purpose is to replace placeholders in configuration files with secret values
#
# Secret string can contain special characters
# Then it should not be used any regular expression engine to not interpret such special characters
#
# Moreover secrets cannot be passed as command line arguments
# It is to not make them leak when listening all run processes by any user in system
#
# References
# https://stackoverflow.com/questions/7970702/simple-search-and-replace-without-regex
# https://www.netmeister.org/blog/passing-passwords.html
#
str_replace() {

    # String to find
    find="$1"

    # File to process
    file="$2"

    # Process input file
    perl \
        \
        `# Run instructions for each input line and print result` \
        -pe \
        '
            # Run initialization block once
            BEGIN {

                # Read replacement string from standard input
                $replace = <STDIN>;

                # Get rid of trailing new line
                chomp $replace;
            };

            # Main processing instructions are rerun for each line of input file

            # Loop until string to replace is still found
            while (($position = index($_, '"'$find'"')) != -1) {

                # Replace found string with its replacement
                substr($_, $position, length('"'$find'"')) = $replace;
            }

        ' \
        \
        `# Process file in place` \
        -i "$file" \

}


# Replace strings but firstly ensure that file is not accessible by any others
secret_fill() {

    # String to find
    find="$1"

    # File to process
    file="$2"

    # Prevent reading by others file with filled secrets
    chmod o-rwx,a-rwx "$file"

    # Replace secret placeholder with proper value
    str_replace "$find" "$file"

}


# Process given file and replace all its placeholders with secrets
# Secrets should be provided on standard input in separate lines in form:
#   placeholder=secret value
# It would replace strings
#   $placeholder
#   ${placeholder}
# Placeholder name has to be one word only
# Placeholder value could be many words and multiline
# Any $<word> would be replaced even with empty string if not defined
all_secrets_fill() {

    # File to process
    file="$1"

    # Start new shell in clean environment
    env \
        \
        `# Remove all environment variables` \
        --ignore-environment \
        \
        `# Only PATH environment variable is preserved` \
        PATH="$PATH" \
        \
        bash \
            -c \
                "
                    # Stop on first failure
                    set -o errexit

                    # Read all secret assigments one by one
                    while read assigment; do

                        # Export assigment as process environment variable
                        # Export is bash builtin function
                        # So it will not fork new process with secret arguments visible by all system users
                        export "'"$assigment"'"

                    done

                    # Replace all secret variables with theirs values
                    # Keep result in memory
                    content_with_secrets="'"$(envsubst < "'"$file"'")"'"

                    # Save result on disk in same location
                    cat > '$file' <<EOF
\$content_with_secrets
EOF
                "
}
