# Core functionality


provision_pre() {

    # Create output directories if not yet done
    mkdir --parent ./output/

    # Run preparation recipe script if exists
    if test -r "$recipe/pre.sh"; then

        # Run in subshell to not affect parent shell
        (

            # Go to working directory
            cd ./output/

            # Run script
            . "$recipe/pre.sh"
        )
    fi

}


provision_post() {

    # Run preparation recipe script if exists
    if test -r "$recipe/post.sh"; then

        # Run script
        . "$recipe/post.sh"

    fi

}


provision_user() {

    # Get directory with recipe
    recipe="$1"

    # Get optional argument to target protocol
    protocol="$2"

    # Get optional argument to target location
    target="$3"

    # Copy files for user home
    if test -d "$recipe/user/"; then
        cp --recursive "$recipe/user/" ./output/
    fi

    # Run user recipe script if exists
    if test -r "$recipe/user.sh"; then

        # Run in subshell to not affect parent shell
        (

            # Go to working directory
            cd ./output/user/

            # Run script for user
            . "$recipe/user.sh"
        )
    fi

    # Install prepared files for user
    if test -d "./output/user/"; then
        _install_user "$protocol" "$target"
    fi

}


_install_user() {

    # Get optional argument to target protocol
    protocol="$1"

    # Get optional argument to target location
    target="$2"

    # Dispatch method to copy final files
    case "$protocol" in

        # Install on local host If not specified otherwise
        "")
            cp --recursive ./output/user/. "$HOME/"
            ;;

        # Install on remote host
        ssh)
            tar c --directory ./output/user/ . | ssh "$target" 'tar x --verbose'
            ;;

        # Test purpose only
        dryrun)
            cat >&2 <<EOF
INFO: dry run target is specified
INFO: do nothing for copying files in target location
EOF
            ;;

        # Handle error
        *)
            echo >&2 "ERROR: Target host protocol is not recognized"
            exit 1
            ;;

    esac

}


provision_system() {

    # Get directory with recipe
    recipe="$1"

    # Get optional argument to target protocol
    protocol="$2"

    # Get optional argument to target location
    target="$3"

    # Copy files for system
    if test -d "$recipe/system/"; then
        cp --recursive "$recipe/system/" ./output/
    fi

    # Run system recipe script if exists
    if test -r "$recipe/system.sh"; then

        # Run in subshell to not affect parent shell
        (

            # Go to working directory
            cd ./output/system/

            # Run script for system
            . "$recipe/system.sh"
        )
    fi

    # Install prepared files for system
    if test -d "./output/system/"; then
        _install_system "$protocol" "$target"
    fi

}


_install_system() {

    # Get optional argument to target protocol
    protocol="$1"

    # Get optional argument to target location
    target="$2"

    # Dispatch method to copy final files
    case "$protocol" in

        # Install on local host If not specified otherwise
        "")
            cp --recursive ./output/system/. /
            ;;

        # Install on remote host
        ssh)
            tar c --directory ./output/system/ . | ssh "$target" 'sudo tar x --verbose  --directory /'
            ;;

        # Test purpose only
        dryrun)
            cat >&2 <<EOF
INFO: dry run target is specified
INFO: do nothing for copying files in target location
EOF
            ;;

        # Handle error
        *)
            echo >&2 "ERROR: Target host protocol is not recognized"
            exit 1
            ;;

    esac

}
