# Provisioner

## Purpose

Simple tool and library to help provision host using provided shell
script with further commands.

### Install external software

It should help to install software from some external source rather then package
manager in distribution to have another version or unavailable one.

### Install configuration

Installed software packages may require some custom configuration.

Configuration can be kept as template with some placeholders to be replaced with
 secret values retrieved from safe place.


## Requirements

It should have minimal dependencies.

It requires network connection to download extra software.

It should be possible to only prepare configuration files on native host for
another system. Then they could be copied into target system somehow.

## Run

To install some software packages and their example custom configurations
``` sh
provisioner example/movie-studio.cfg
provisioner example/openvpn-home-server.cfg
provisioner example/emacs-doom-rust.cfg
```

Could be also run from network without cloning repository
``` sh
curl https://gitlab.com/provisioner-light/provisioner-light/example/openvpn-home-server.cfg > openvpn-home-server.cfg
curl https://gitlab.com/provisioner-light/provisioner-light/provisioner | sh -s -- openvpn-home-server.cfg
```

## Dependencies

### Mandatory
- `bash`
- `coreutils`

### Optional
- `wget` or `curl` to download extra files
- `perl` to replace strings in configuration files templates
- `pass` to retrieve secrets
